# **README** #

Ultimo projeto da enfase 3 de ciências da computação (e ultimo período: 9) onde o projeto é criar um protótipo para avisar se as vagas do estacionamento de veículos estão disponível ou não.

Envolveu um servidor, mobiles (IOS, Windows Phone e Android),aplicativo web e alguns arduino. Utilizamos uma maquete para a apresentação. A equipe era de uns 8 alunos, cada um fazendo sua parte individual (hardware e software). Toda comunicação é criptografada.

Eu fiquei no desenvolvimento do aplicativo no android onde implementei as interfaces de usuário, logica da aplicação e a comunicação com servidor via SOAP ou seja, JSON.
Outros ficaram no servidor, outros mobile, hardware e documentação.

Funcionamento simples, um veiculo entra na vaga e o aplicativo web e mobile muda o estado da vaga.

Fotos: https://drive.google.com/folderview?id=0B7siHNeG5zjhfk9zWnlLam5kZktJZXJ0U09MekJSZml4NWFubFdoVUpkSWtlVC1HT3ZUY1k