package com.pucgo.tatooine;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

public class VagasActivity extends Activity implements MessageHandler
{
	private ProgressDialog progress;

	GridView gridView;
	MyApplication myApplication;

	MyArrayAdapter<Vaga> vagasAdapt;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss a");

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vagas);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		progress = new ProgressDialog(this);
		progress.setMessage("Atualizando...");
		progress.setTitle("Vagas");
		progress.setCancelable(false);

		myApplication = (MyApplication) this.getApplication();
		gridView = (GridView) findViewById(R.id.vagaslistView);
		setTitle("Estacionamentos: " + myApplication.estacionamentoSelecionado.nome);
		// selectedArea.vagasList.add(new Vaga()); // teste

		// System.out.println("SIZE: " + selectedArea.vagasList.size());

		vagasAdapt = new MyArrayAdapter<Vaga>(this, R.layout.grid_layout, myApplication.estacionamentoSelecionado.vagas);
		gridView.setAdapter(vagasAdapt);

		if(myApplication.estacionamentoSelecionado.vagas.size() > 0)
		{
			TextView textView = (TextView) findViewById(R.id.txt_serverUpdate2);
			textView.setText("Atualizado em: " + myApplication.estacionamentoSelecionado.ultimaAtualizacao);
		}
		else
		{ // atualiza sozinho se n�o tiver vagas carregado ainda
			myApplication.newMessage(METHOD.LIST_VAGAS, VagasActivity.this, myApplication.selectedArea.nome, myApplication.estacionamentoSelecionado.nome);
		}

		Button buttonAttVagas = (Button) findViewById(R.id.btn_AttVagas);

		buttonAttVagas.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				myApplication.newMessage(METHOD.LIST_VAGAS, VagasActivity.this, myApplication.selectedArea.nome, myApplication.estacionamentoSelecionado.nome);
			}
		});

	}

	@Override
	public void messageStarted(METHOD method)
	{
		progress.show();
	}

	@Override
	public void messageFinished(METHOD method, STATE state, String rawdata)
	{

		System.out.println(rawdata);

		JSONObject jsonObject;
		try
		{
			jsonObject = new JSONObject(rawdata);

			boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
			if (success)
			{
				String totalVagasStr = jsonObject.getString("total"); // total de areas
				int totalVagas = Integer.parseInt(totalVagasStr);

				myApplication.estacionamentoSelecionado.vagas.clear();

				if (totalVagas > 0)
				{
					JSONArray objData = jsonObject.getJSONArray("data");

					for (int i = 0; i < objData.length(); i++)
					{
						JSONObject vagaJson = objData.getJSONObject(i);

						String estacionamento = vagaJson.getString("estacionamento");
						String numeroVaga = vagaJson.getString("numeroVaga");
						String corredor = vagaJson.getString("numeroVaga");
						String id = vagaJson.getString("id");
						String statusStr = vagaJson.getString("status");
						boolean statusDisponivel = statusStr.equals("L") ? true : false; // Livre = true.

						Vaga vaga = new Vaga();
						vaga.numeroVaga = numeroVaga;
						vaga.disponivel = statusDisponivel;
						myApplication.estacionamentoSelecionado.vagas.add(vaga);

					}

					vagasAdapt.notifyDataSetChanged();

					TextView textView = (TextView) findViewById(R.id.txt_serverUpdate2);

					Calendar calendar = Calendar.getInstance();
					final String strDate = simpleDateFormat.format(calendar.getTime());
					myApplication.estacionamentoSelecionado.ultimaAtualizacao = strDate;
					textView.setText("Atualizado em: " + strDate);

				}
				else
				{
					Toast toast = Toast.makeText(VagasActivity.this, "Nenhuma vaga disponivel", Toast.LENGTH_SHORT);
					toast.show();
				}

			}
			else
			{
				Toast toast = Toast.makeText(VagasActivity.this, "Error no servidor", Toast.LENGTH_SHORT);
				toast.show();
			}
		}
		catch (JSONException e)
		{
			Toast toast = Toast.makeText(VagasActivity.this, "Error ao atualizar Vagas", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}

		progress.dismiss();
	}

	@Override
	public void messageError(METHOD method)
	{
		Toast toast = Toast.makeText(VagasActivity.this, "Error no servi�o", Toast.LENGTH_SHORT);
		toast.show();
		progress.dismiss();
	}

}
