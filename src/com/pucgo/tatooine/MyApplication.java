package com.pucgo.tatooine;

import org.json.JSONException;
import org.json.JSONObject;

import com.Wsdl2Code.WebServices.ExampleServiceService.ExampleServiceService;

import android.app.Application;
import android.os.AsyncTask;
import android.os.StrictMode;

public class MyApplication extends Application
{
	String emailLogin;
	
	
	public static boolean cript = true;
	
	public static enum METHOD
	{
		LOGIN, LIST_AREA, LIST_ESTACIONAMENTOS, LIST_VAGAS, CADASTRO, ALTERAR_CADASTRO, RECUPERAR_CADASTRO, VERIFICAR_EMAIL_EXISTE
	}

	public static enum STATE
	{
		SUCCESS, ERROR
	}

	Area selectedArea;
	Estacionamento estacionamentoSelecionado;
	
	
	ExampleServiceService service;

	@Override
	public void onCreate()
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		super.onCreate();
		AES.setKey("pucgoenfase20142");
		service = new ExampleServiceService();
	}

	public void newMessage(final METHOD method, final MessageHandler handler, final String... params)
	{
		if (handler == null)
			return;

		new AsyncTask<Void, Void, String>()
		{
			@Override
			protected void onPreExecute()
			{
				// starting
				handler.messageStarted(method);
			};

			@Override
			protected String doInBackground(Void... paramVoid)
			{
				String obj = null;

				if (method == METHOD.LOGIN)
				{
					if (params.length == 2)
					{
						String param1 = AES.encrypt(params[0]);
						String param2 = AES.encrypt(params[1]);
						if(cript)
							obj = service.login(param1, param2);
						else
							obj = service.login(params[0],params[1]);
					}
					
				}
				else if(method == METHOD.LIST_AREA)
				{
					obj = service.recuperarAreas();
				}
				else if(method == METHOD.LIST_ESTACIONAMENTOS)
				{
					if (params.length == 1)
					{
						if(cript)
							obj = service.recuperarEstacionamentoPorArea(AES.encrypt(params[0]));
						else
							obj = service.recuperarEstacionamentoPorArea(params[0]);
					}
				}
				else if(method == METHOD.LIST_VAGAS)
				{
					if (params.length == 2)
					{
						if(cript)
							obj = service.recuperarVagasPorAreaEstacionamento(AES.encrypt(params[0]), AES.encrypt(params[1]));
						else
							obj = service.recuperarVagasPorAreaEstacionamento(params[0], params[1]);
					}
				}
				else if(method == METHOD.CADASTRO)
				{
					if (params.length == 5)
					{	
						if(cript)
						{
							String param4 =  params[4].isEmpty() == true ? "" : AES.encrypt(params[4]);
							obj = service.cadastrar(AES.encrypt(params[0]), AES.encrypt(params[1]), AES.encrypt(params[2]), AES.encrypt(params[3]), param4);
						}
						else
							obj = service.cadastrar(params[0], params[1], params[2], params[3], params[4]);
						
					}
				}
				else if(method == METHOD.ALTERAR_CADASTRO)
				{
					if (params.length == 5)
					{
						if(cript)
						{
							//service.alterarCadastro(nome, email, matricula);
						}
						else
						{
							
						}
					}
				}
				else if(method == METHOD.RECUPERAR_CADASTRO)
				{
					if (params.length == 1)
					{
						if(cript)
						{
							service.recuperarDados(AES.encrypt(params[0]));
						}
						else
						{
							service.recuperarDados(params[0]);
						}
					}
				}
				else if(method == METHOD.VERIFICAR_EMAIL_EXISTE)
				{
					if (params.length == 1)
					{	
						if(cript)
							obj = service.verificarSeEmailExiste(AES.encrypt(params[0]));
						else
							obj = service.verificarSeEmailExiste(params[0]);
					}
				}


				
				if(cript)
				{
					JSONObject jsonObject = null;
					String encrypt = null;
					try {
						jsonObject = new JSONObject(obj);
						encrypt = jsonObject.getString("encrypt");
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					
					
					AES.decrypt(encrypt);
					obj = AES.getDecryptedString();
				}
				
				
				return obj;
			}

			@Override
			protected void onPostExecute(String result)
			{

				if (result != null && result.isEmpty() == false)
				{
					handler.messageFinished(method, STATE.SUCCESS, result);
				}
				else
				{
					handler.messageError(method);
				}
			}
		}.execute();
	}

}
