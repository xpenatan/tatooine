package com.pucgo.tatooine;

import org.json.JSONException;
import org.json.JSONObject;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class CadastroActivity extends Activity implements MessageHandler
{
	public static int ALTERAR_CADASTRO = 1;
	public static int CADASTRAR = 2;

	MyApplication myApplication;

	private ProgressDialog progress;

	int index = 2;

	boolean alterarCadastro = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		myApplication = (MyApplication) this.getApplication();

		progress = new ProgressDialog(this);

		progress.setTitle("Cadastro");
		progress.setCancelable(false);

		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			int value = extras.getInt("Cadastro");

			if (value == ALTERAR_CADASTRO)
			{
				alterarCadastro = true;
				myApplication.newMessage(METHOD.RECUPERAR_CADASTRO, CadastroActivity.this, myApplication.emailLogin);
				progress.setMessage("Carregando Cadastro...");
			}
			else
			{
				alterarCadastro = false;
			}
		}

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupMatricula);

		final TextView textMat = (TextView) findViewById(R.id.textViewMatricula);
		final EditText cadastroNome = (EditText) findViewById(R.id.editText_CadastroNome);
		final EditText cadastroEmail = (EditText) findViewById(R.id.editText_CadastroEmail);
		final EditText cadastroSenha = (EditText) findViewById(R.id.editText_CadastroSenha);
		final EditText cadastroConfirmaSenha = (EditText) findViewById(R.id.editText_CadastroConfSenha);
		final EditText cadastroMatricula = (EditText) findViewById(R.id.editText_CadastroMatricula);

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				if (checkedId == R.id.radio_aluno)
				{
					index = 0;
					textMat.setVisibility(View.VISIBLE);
					cadastroMatricula.setVisibility(View.VISIBLE);
				}
				else if (checkedId == R.id.radio_Prof)
				{
					index = 1;
					textMat.setVisibility(View.VISIBLE);
					cadastroMatricula.setVisibility(View.VISIBLE);
				}
				else if (checkedId == R.id.radio_Visitante)
				{
					index = 2;
					textMat.setVisibility(View.INVISIBLE);
					cadastroMatricula.setVisibility(View.INVISIBLE);
				}
				else
				{

				}
			}

		});

		Button confirma_btn = (Button) findViewById(R.id.cadastroConfirma_btn);

		confirma_btn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				String senha = cadastroSenha.getText().toString();

				if (senha.length() >= 6)
				{
					String confirmaSenha = cadastroConfirmaSenha.getText().toString();

					if (confirmaSenha.length() >= 6 && confirmaSenha.contentEquals(senha))
					{
						String nome = cadastroNome.getText().toString();

						if (nome.length() >= 3)
						{
							String email = cadastroEmail.getText().toString();

							if (email.length() >= 3)
							{

								if (index == 2)
								{ // visitante, nao envia matricula V

									if (alterarCadastro)
										progress.setMessage("Alterando Cadastro...");
									else
										progress.setMessage("Cadastrando...");

									myApplication.newMessage(METHOD.CADASTRO, CadastroActivity.this, nome, email, senha, "V", "");
								}
								else if (index == 1)
								{ // prof P
									String matricula = cadastroMatricula.getText().toString();

									if (matricula.length() > 0)
									{
										if (alterarCadastro)
										{
											progress.setMessage("Alterando Cadastro...");
										}
										else
										{
											progress.setMessage("Cadastrando...");
											myApplication.newMessage(METHOD.CADASTRO, CadastroActivity.this, nome, email, senha, "P", matricula);
										}
									}
									else
									{
										Toast toast = Toast.makeText(CadastroActivity.this, "Matricula incorreta", Toast.LENGTH_SHORT);
										toast.show();
									}

								}
								else
								{ // aluno A

									String matricula = cadastroMatricula.getText().toString();
									if (alterarCadastro)
										progress.setMessage("Alterando Cadastro...");
									else
										progress.setMessage("Cadastrando...");

									if (matricula.length() > 0)
									{
										myApplication.newMessage(METHOD.CADASTRO, CadastroActivity.this, nome, email, senha, "A", matricula);
									}
									else
									{
										Toast toast = Toast.makeText(CadastroActivity.this, "Matricula incorreta", Toast.LENGTH_SHORT);
										toast.show();
									}
								}
							}
							else
							{
								Toast toast = Toast.makeText(CadastroActivity.this, "Email contem poucos caracteres", Toast.LENGTH_SHORT);
								toast.show();
							}

						}
						else
						{
							Toast toast = Toast.makeText(CadastroActivity.this, "Nome contem poucos caracteres", Toast.LENGTH_SHORT);
							toast.show();
						}
					}
					else
					{
						Toast toast = Toast.makeText(CadastroActivity.this, "Senha de confirma��o incorreto", Toast.LENGTH_SHORT);
						toast.show();
					}
				}
				else
				{
					Toast toast = Toast.makeText(CadastroActivity.this, "Senha tem que 6 ou mais caracteres", Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});

	}

	@Override
	public void messageStarted(METHOD method)
	{
		progress.show();
	}

	@Override
	public void messageFinished(METHOD method, STATE state, String rawData)
	{
		JSONObject jsonObject;
		
		System.out.println(rawData);
		try
		{
			jsonObject = new JSONObject(rawData);

			boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
			if (success)
			{
				Toast toast = Toast.makeText(CadastroActivity.this, "Cadastrado com sucesso ", Toast.LENGTH_SHORT);
				toast.show();
				progress.dismiss();
				finish();
			}
			else
			{
				Toast toast = Toast.makeText(CadastroActivity.this, "Cadastrado ja existe ", Toast.LENGTH_SHORT);
				toast.show();
				progress.dismiss();
			}
		}
		catch (JSONException e)
		{
			Toast toast = Toast.makeText(CadastroActivity.this, "Error ao cadastrar", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}

		
	}

	@Override
	public void messageError(METHOD method)
	{
		Toast toast = Toast.makeText(CadastroActivity.this, "Error no servi�o", Toast.LENGTH_SHORT);
		toast.show();
		progress.dismiss();
	}

}
