package com.pucgo.tatooine;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

public interface MessageHandler
{
	public void messageStarted(METHOD method);
	public void messageFinished(METHOD method, STATE state, String rawData);
	public void messageError(METHOD method);
}
