package com.pucgo.tatooine;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

public class MainActivity extends Activity implements MessageHandler
{
	GridView gridView;
	MyApplication myApplication;

	private ProgressDialog progress;

	ArrayList<Area> areasList = new ArrayList<Area>();
	MyArrayAdapter<Area> adapter;

	MyTimerTask myTimerTask;

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss a");

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		progress = new ProgressDialog(this);
		progress.setMessage("Atualizando...");
		progress.setTitle("Areas");
		progress.setCancelable(false);

		myApplication = (MyApplication) this.getApplication();
		gridView = (GridView) findViewById(R.id.areaslistView);

		adapter = new MyArrayAdapter<Area>(this, R.layout.grid_layout, areasList);

		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
			{
				myApplication.selectedArea = areasList.get(position);
				myApplication.estacionamentoSelecionado = null;
				
				Intent myIntent = new Intent(MainActivity.this, EstacionamentoActivity.class);
				MainActivity.this.startActivity(myIntent);
				MainActivity.this.overridePendingTransition(R.anim.open_next, R.anim.close_main);
			}
		});

		// myTimerTask = new MyTimerTask();
		// timer.schedule(myTimerTask, 1000, 10000);

		Button button = (Button) findViewById(R.id.btn_AltCadastro);
		button.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				Intent myIntent = new Intent(MainActivity.this, CadastroActivity.class);
				myIntent.putExtra("Cadastro", CadastroActivity.ALTERAR_CADASTRO);
				MainActivity.this.startActivity(myIntent);
				MainActivity.this.overridePendingTransition(R.anim.open_next, R.anim.close_main);
			}
		});

		Button buttonAttArea = (Button) findViewById(R.id.btn_AttArea);

		buttonAttArea.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				myApplication.newMessage(METHOD.LIST_AREA, MainActivity.this);
			}
		});
		myApplication.newMessage(METHOD.LIST_AREA, MainActivity.this);

	}

	class MyTimerTask extends TimerTask
	{

		@Override
		public void run()
		{
			new AsyncTask<Void, Void, Void>()
			{

				@Override
				protected Void doInBackground(Void... params)
				{
					myApplication.newMessage(METHOD.LIST_AREA, MainActivity.this);
					return null;
				}
			}.execute();
		}

	}

	@Override
	public void messageStarted(METHOD method)
	{
		progress.show();
	}

	@Override
	public void messageFinished(METHOD method, STATE state, String rawdata)
	{
		System.out.println(rawdata);
		JSONObject jsonObject;
		try
		{
			jsonObject = new JSONObject(rawdata);

			boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
			if (success)
			{
				String totalAreasStr = jsonObject.getString("total"); // total de areas
				int totalAreas = Integer.parseInt(totalAreasStr);

				areasList.clear();

				if (totalAreas > 0)
				{
					JSONArray objData = jsonObject.getJSONArray("data");

					for (int i = 0; i < objData.length(); i++)
					{
						JSONObject areaJson = objData.getJSONObject(i);

						String nomeArea = areaJson.getString("area");
						String totalDisponivelStr = areaJson.getString("totalDisponivel");

						Area area = new Area();
						area.setNome(nomeArea);
						area.setVagasDisponiveis(totalDisponivelStr);
						areasList.add(area);
					}

					adapter.notifyDataSetChanged();


					TextView textView = (TextView) findViewById(R.id.txt_serverUpdate1);

					Calendar calendar = Calendar.getInstance();
					// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss a");

					final String strDate = simpleDateFormat.format(calendar.getTime());
					textView.setText("Atualizado em: " + strDate);

				}
				else
				{
					Toast toast = Toast.makeText(MainActivity.this, "Nenhuma area disponivel", Toast.LENGTH_SHORT);
					toast.show();
				}

			}

		}
		catch (JSONException e)
		{
			Toast toast = Toast.makeText(MainActivity.this, "Error ao atualizar Area", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}

		progress.dismiss();
	}

	@Override
	public void messageError(METHOD method)
	{
		Toast toast = Toast.makeText(MainActivity.this, "Error no servi�o", Toast.LENGTH_SHORT);
		toast.show();
		progress.dismiss();
	}

}
