package com.pucgo.tatooine;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MyArrayAdapter<T> extends ArrayAdapter<T>
{

	public MyArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, 0, objects);
    }
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = super.getView(position, convertView, parent);	
		T t = getItem(position);
		
		
		boolean disponivel = false;
		
		
		if(t instanceof Vaga)
		{
			if(((Vaga) t).disponivel)
			{
				disponivel = true;
			}
		}
		else if(t instanceof Estacionamento)
		{
			int vagasDisponivel = Integer.parseInt(((Estacionamento) t).vagasDisponiveis);
			if(vagasDisponivel > 0)
				disponivel = true;
		}
		else if(t instanceof Area)
		{
			int vagasDisponivel = Integer.parseInt(((Area) t).vagasDisponiveis);
			if(vagasDisponivel > 0)
				disponivel = true;
		}
		
		if(disponivel)
			view.setBackgroundColor(Color.argb(255, 0, 150, 0));
		else
			view.setBackgroundColor(Color.RED);
		
		
		return view;
	}

}
