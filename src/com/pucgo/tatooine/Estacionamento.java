package com.pucgo.tatooine;

import java.util.ArrayList;

public class Estacionamento
{
	String ultimaAtualizacao;
	String nome; // nome do estacionamento
	String vagasDisponiveis;
	ArrayList<Vaga> vagas = new ArrayList<Vaga>();
	
	@Override
	public String toString()
	{
		return nome + ": " + vagasDisponiveis + " Vagas(s)";
	}
	
}
