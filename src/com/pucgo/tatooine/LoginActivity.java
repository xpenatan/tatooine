package com.pucgo.tatooine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

public class LoginActivity extends Activity implements MessageHandler
{
	MyApplication myApplication;

	private ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		myApplication = (MyApplication) this.getApplication();

		View viw = findViewById(R.id.login_btn);
		Button button = (Button)viw ;

		progress = new ProgressDialog(this);
		progress.setMessage("Connectando...");
		progress.setTitle("Login");
		progress.setCancelable(false);

		button.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

				inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

				EditText username_text = (EditText) findViewById(R.id.editText_Login);
				EditText password_text = (EditText) findViewById(R.id.editText_Password);

				String username_str = username_text.getText().toString();
				String password_str = password_text.getText().toString();

				if (username_str != null && !username_str.isEmpty() && password_str != null && !password_str.isEmpty())
				{
					System.out.println(username_str + " + " + password_str);
					myApplication.emailLogin = username_str; // usado somente quando passar da tela login
					myApplication.newMessage(METHOD.LOGIN, LoginActivity.this, username_str, password_str);
				}

			}
		});
		
		View viw2 = findViewById(R.id.btn_newUser);
		Button button2 = (Button)viw2 ;
		
		
		button2.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				Intent myIntent = new Intent(LoginActivity.this, CadastroActivity.class);
				myIntent.putExtra("Cadastro", CadastroActivity.CADASTRAR);
				LoginActivity.this.startActivity(myIntent);
				LoginActivity.this.overridePendingTransition(R.anim.open_next, R.anim.close_main);
			}
		});
	}

	@Override
	public void messageStarted(METHOD method)
	{
		progress.show();
	}

	@Override
	public void messageFinished(METHOD method, STATE state, String json)
	{
		System.out.println(json);
		JSONObject jsonObject;
		
		
		
		
			try
			{
				jsonObject = new JSONObject(json);
				
				boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
				if (success) // servidor nao deu problema
				{
					JSONArray data = jsonObject.getJSONArray("data");
					// JSONParserr.getData(data);
					String usuarioValido = data.getJSONObject(0).getString("usuarioValido");
	
					if (usuarioValido.equals("1"))
					{
						Toast toast = Toast.makeText(LoginActivity.this, "Connectado", Toast.LENGTH_SHORT);
						toast.show();
	
						Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
						LoginActivity.this.startActivity(myIntent);
						LoginActivity.this.overridePendingTransition(R.anim.open_next, R.anim.close_main);
						LoginActivity.this.finish();
	
					}
					else
					{
						Toast toast = Toast.makeText(LoginActivity.this, "Login invalido", Toast.LENGTH_SHORT);
						toast.show();
					}
				}
				else
				{
					Toast toast = Toast.makeText(LoginActivity.this, "Error no servidor", Toast.LENGTH_SHORT);
					toast.show();
				}
	
			}
			catch (JSONException e)
			{
				Toast toast = Toast.makeText(LoginActivity.this, "Error ao fazer login", Toast.LENGTH_SHORT);
				toast.show();
				e.printStackTrace();
			}

		progress.dismiss();
	}

	@Override
	public void messageError(METHOD method)
	{
		
		Toast toast = Toast.makeText(LoginActivity.this, "Error no servi�o", Toast.LENGTH_SHORT);
		toast.show();
		
		progress.dismiss();
	}

}
