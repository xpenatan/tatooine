package com.pucgo.tatooine;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pucgo.tatooine.MyApplication.METHOD;
import com.pucgo.tatooine.MyApplication.STATE;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class EstacionamentoActivity extends Activity implements MessageHandler
{

	GridView gridView;
	MyApplication myApplication;
	
	private ProgressDialog progress;
	
	
	MyArrayAdapter<Estacionamento> adapter;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss a");
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_estacionamento);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		progress = new ProgressDialog(this);
		progress.setMessage("Atualizando...");
		progress.setTitle("Estacionamentos");
		progress.setCancelable(false);
		
		myApplication = (MyApplication) this.getApplication();
		gridView = (GridView) findViewById(R.id.areasEstacionamentoView);
		adapter = new MyArrayAdapter<Estacionamento>(this, R.layout.grid_layout, myApplication.selectedArea.estacionamentos);
		gridView.setAdapter(adapter);
		
		gridView.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
			{
				myApplication.estacionamentoSelecionado = myApplication.selectedArea.estacionamentos.get(position);
				Intent myIntent = new Intent(EstacionamentoActivity.this, VagasActivity.class);
				EstacionamentoActivity.this.startActivity(myIntent);
				EstacionamentoActivity.this.overridePendingTransition(R.anim.open_next, R.anim.close_main);
			}
		});
		
		Button button = (Button) findViewById(R.id.btn_AttEstacionamento);
		button.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				myApplication.newMessage(METHOD.LIST_ESTACIONAMENTOS, EstacionamentoActivity.this, myApplication.selectedArea.nome);
			}
		});
		
		
		if(myApplication.selectedArea.estacionamentos.size() > 0)
		{
		}
		else
		{ // atualiza sozinho se n�o tiver vagas carregado ainda
			myApplication.newMessage(METHOD.LIST_ESTACIONAMENTOS, EstacionamentoActivity.this, myApplication.selectedArea.nome);
		}
	}
	

	@Override
	public void messageStarted(METHOD method)
	{
		progress.show();
	}

	@Override
	public void messageFinished(METHOD method, STATE state, String rawdata)
	{
		System.out.println(rawdata);
		
		
		JSONObject jsonObject;
		try
		{
			jsonObject = new JSONObject(rawdata);

			boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
			if (success)
			{
				String totalStr = jsonObject.getString("total"); // total de areas
				int totalEstacionamentos = Integer.parseInt(totalStr);

				myApplication.selectedArea.estacionamentos.clear();

				if (totalEstacionamentos > 0)
				{
					JSONArray objData = jsonObject.getJSONArray("data");

					for (int i = 0; i < objData.length(); i++)
					{
						JSONObject vagaJson = objData.getJSONObject(i);

						String estacionamentoStr = vagaJson.getString("estacionamento");
						String totalDisponivelStr = vagaJson.getString("totalDisponivel");
				
						Estacionamento estacionamento = new Estacionamento();
						estacionamento.nome = estacionamentoStr;
						estacionamento.vagasDisponiveis = totalDisponivelStr;
						myApplication.selectedArea.estacionamentos.add(estacionamento);

					}

					adapter.notifyDataSetChanged();

					//TextView textView = (TextView) findViewById(R.id.txt_serverUpdate2);

					Calendar calendar = Calendar.getInstance();
					final String strDate = simpleDateFormat.format(calendar.getTime());
					myApplication.selectedArea.ultimaAtualizacao = strDate;
					//textView.setText("Atualizado em: " + strDate);

				}
				else
				{
					Toast toast = Toast.makeText(EstacionamentoActivity.this, "Nenhum estacionamento disponivel", Toast.LENGTH_SHORT);
					toast.show();
				}

			}
			else
			{
				Toast toast = Toast.makeText(EstacionamentoActivity.this, "Error no servidor", Toast.LENGTH_SHORT);
				toast.show();
			}

		}
		catch (JSONException e)
		{
			Toast toast = Toast.makeText(EstacionamentoActivity.this, "Error ao atualizar Vagas", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}
		
		
		progress.dismiss();
	}

	@Override
	public void messageError(METHOD method)
	{
		Toast toast = Toast.makeText(EstacionamentoActivity.this, "Error no servi�o", Toast.LENGTH_SHORT);
		toast.show();
		progress.dismiss();
	}


}
