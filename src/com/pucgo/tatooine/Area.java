package com.pucgo.tatooine;

import java.util.ArrayList;

public class Area
{
	String ultimaAtualizacao;
	String nome;
	String vagasDisponiveis;
	
	
	ArrayList<Estacionamento> estacionamentos = new ArrayList<Estacionamento>();
	
	
	public String getName()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	
	public String getVagasDisponiveis()
	{
		return vagasDisponiveis;
	}
	public void setVagasDisponiveis(String vagasDisponiveis)
	{
		this.vagasDisponiveis = vagasDisponiveis;
	}
	public String getUltimaAtualizacao()
	{
		return ultimaAtualizacao;
	}
	public void setUltimaAtualizacao(String ultimaAtualizacao)
	{
		this.ultimaAtualizacao = ultimaAtualizacao;
	}
	
	@Override
	public String toString()
	{
		return nome + ": " + vagasDisponiveis + " Vagas(s)";
	}
	
}
